// https://docs.renovatebot.com/configuration-options
module.exports = {
  platform: "gitlab",
  endpoint: "https://gitlab.com/api/v4/",

  repositories: ["dsbert/renovate-bot"],

  requireConfig: true,
  onboarding: true,
  onboardingConfig: {
    extends: ["config:base"],
  },

  enabledManagers: ["npm"],
};
